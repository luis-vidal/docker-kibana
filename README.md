## Docker configuration for ELK ##
* Deploy all the ELK containers
**docker-compose -f docker-compose.yml up -d**

* See the labels of a container
**docker inspect -f '{{ range $k, $v := .Config.Labels -}}{{ $k }}={{ $v }}{{ end -}}' f9bf2127a2fa**


* Para acceder a kibana solo hay que ir a 
https://www.elastic.co/guide/en/apm/get-started/current/quick-start-overview.html


* Documentación docker-compose con APM
https://www.elastic.co/guide/en/apm/get-started/current/quick-start-overview.html